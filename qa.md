- Назови типы dns записей которые вспомнишь
  1. A
  2. MX+
  3. Cname
  4. AAAA+
  5. TXT
  6. SRV
  7. spf,dkim,dmarc - ошибка, тк это записи типа TXT
- Что такое NAT?
  1. Network Address Translation - это механизм в сетях TCP/IP, позволяющий изменять IP адрес в заголовке пакета, проходящего через устройство маршрутизации трафика.
- как посмотреть последние 99 строк файла в linux
  1. tail -n 99 file
- \* как на одном сервере запустить одновременно apache и nginx на 80 порту?
  1. заставить слушать разные сетевые интерфейсы
- как выйти из vim
  1. :q! 
- как показать каждый элемент списка 1,2,3,4,5 ? на любом языке , нужно чтоб в чат написал.
  1. for i in [1,2,3,4,5,6]: print(i) | python
  2. LIST = (1 2 3 4 5); for i in ***$LIST[@]***; do echo $i; done | bash (в баше важно понимать метод обращения к списку)
- чем отличается список  (list) от словаря (dict)? в разрезе пинтона (если есть в резюме Python/Ansible)
  1. dict - коллекции произвольных объектов с ***доступом по ключу***
  2. list - ***упорядоченные*** изменяемые коллекции объектов произвольных типов
  3. в кратце: list - список нумерованых обектов, dict - список ключ-значение
- чем отличается 23 и "23"?
  1. 23 - число (type number) "23" - строка (type string)


____
вопрос про vim это старая шутка с бородой до пекина.
она должна просто улыбать тех кто в теме, или не вызывать эмоций. А когда реагируюn бурно, разводя разговор об этом на 5 минут, значит это единственное чем можно козырнуть.



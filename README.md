# Tasks


### ansible
* установить пакеты:
  - nginx
  - haproxy
  - bind
* насторить bind для домена company.loc (взять **ВСЕ** хосты из инвентори)
* настроить bind публичные имена {dev,stage.prod}.example.com
* Запустить nginx
* Настроить haproxy на проксирование запросов к nginx

### схема
``` mermaid
flowchart LR
wan[internet] --> hp[haproxy]
hp[haproxy] --> ng{nginx}
names[names: <br>dev.example.com<br>stage.example.com<br>prod.example.com]
hp <-.- names;
```
